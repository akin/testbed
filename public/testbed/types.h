
#ifndef TESTBED_TYPES_H_
#define TESTBED_TYPES_H_

#include "common.h"
#include <vector>
#include <unordered_map>

struct sqlite3;

namespace TestBed 
{
	class Transform
	{
	public:
		glm::vec3 location;
		glm::vec3 scale;
		glm::quat rotation;
		bool dirty;

		Transform()
		: location{ 0,0,0 }
		, scale{ 1,1,1 }
		, rotation{ 0.f,0.f, 0.f, 0.f }
		, dirty(true)
		{
		}
	};

	using EntityID = size_t;
	class TransformData
	{
	public:
		std::vector<glm::vec3> location;
		std::vector<glm::vec3> scale;
		std::vector<glm::quat> rotation;
		std::vector<bool> dirty; // !!!!! Does this go into the vector<bool> hack???
		std::vector<glm::mat4> localMatrix;
		std::vector<glm::mat4> worldMatrix;
		std::vector<EntityID> entityId;
		std::vector<size_t> parent; // points to indexes in the vectors.
		std::vector<std::vector<size_t>> childs; // points to indexes in the vectors.
		std::unordered_map<EntityID, size_t> entityMap; // map of indexes
	};

	enum class Type
	{
		boolean,
		uint32,
		uint64,
		int32,
		int64,
		string,
		float32,
		float64,
		vec2,
		vec3,
		vec4,
		veci2,
		veci3,
		veci4
	};

	class NamedType
	{
	public:
		NamedType(const std::string& name, Type type)
		: name(name)
		, type(type)
		{}

		std::string name;
		Type type;
	};

	enum class SerializationFormat
	{
		Json,
		Csv
	};
} // ns TestBed

#endif // TESTBED_TYPES_H_
