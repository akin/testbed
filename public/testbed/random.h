
#ifndef TESTBED_RANDOM_H_
#define TESTBED_RANDOM_H_

#include <random>

namespace TestBed 
{
	class Random
	{
	private:
		std::random_device device;
		std::mt19937 engine;
		std::uniform_real_distribution<double> distribution;
	public:
		Random(double min, double max)
		: engine(device())
		, distribution(min, max)
		{
		}

		float get()
		{
			return (float)distribution(engine);
		}
	};
} // ns TestBed

#endif // TESTBED_RANDOM_H_
