
#ifndef TESTBED_UTIL_H_
#define TESTBED_UTIL_H_

#include <string>
#include <thread>

namespace TestBed 
{
bool setAffinity(std::thread& thread, size_t core);
std::string toString(double number);
} // ns TestBed

#endif // TESTBED_UTIL_H_
